/*
SQLyog Professional v12.4.1 (64 bit)
MySQL - 10.1.31-MariaDB : Database - smart
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`smart` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `smart`;

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `nim` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `fakultas` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`id`,`nama`,`nim`,`jenis_kelamin`,`fakultas`) values 
(1,'Johny Pambudi','1234567890','L','Teknik'),
(2,'Maya Rahmaniah','1234456667','P','Ekonomi'),
(3,'Diki ALfarabi Hadi','123345678887','L','Teknik'),
(4,'Suamtono','123456789','L','Fisip'),
(5,'Jamludin Syah','12345663536','L','Teknik'),
(6,'Rahmaniah','212111231133','P','Ekonomi'),
(7,'Qiano Arfabian Putra','1123555365','L','Teknik'),
(8,'Gibran','1122432434','L','Ekonomi'),
(9,'Johny','12363377332','L','Pertanian'),
(10,'Muhammad Riski','12837373839','L','Fisip'),
(11,'Rahmat Syah Alub','122652626252','L','Fisip'),
(12,'Mahmud Amir','123455467464','L','Pertanian'),
(13,'Aminah','123112342','P','Teknik'),
(14,'Putri Aladin','213114324234','P','Ekonomi'),
(15,'Lubis','11231334234','P','Pertanian'),
(16,'Iqlima','12312423423','P','Pertanian'),
(17,'Rahman Muhammad','121312438','L','Pertanian'),
(18,'Muhammad Ikbal','12387448844','L','Teknik'),
(19,'Monika','1223244344','P','Fisip'),
(20,'Haris Aziz','1123834748','L','');

/*Table structure for table `master_question` */

DROP TABLE IF EXISTS `master_question`;

CREATE TABLE `master_question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `soal` text,
  `f1` varchar(40) DEFAULT NULL,
  `f2` varchar(40) DEFAULT NULL,
  `f3` varchar(40) DEFAULT NULL,
  `f4` varchar(40) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `master_question` */

insert  into `master_question`(`id_question`,`soal`,`f1`,`f2`,`f3`,`f4`,`date`) values 
(1,'K','NKN','KNK','NKN','KNK','2019-01-01 13:08:28');

/*Table structure for table `register` */

DROP TABLE IF EXISTS `register`;

CREATE TABLE `register` (
  `id_reg` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` text,
  `foto` text,
  `waktu` datetime DEFAULT NULL,
  PRIMARY KEY (`id_reg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `register` */

insert  into `register`(`id_reg`,`nama`,`email`,`password`,`foto`,`waktu`) values 
(0,'fauzi','fauzi@gmail.com','123','1.jpg','2019-01-01 19:52:54');

/*Table structure for table `results` */

DROP TABLE IF EXISTS `results`;

CREATE TABLE `results` (
  `id_res` int(11) NOT NULL AUTO_INCREMENT,
  `id_reg` int(11) DEFAULT NULL,
  `ce` double DEFAULT NULL,
  `ro` double DEFAULT NULL,
  `ac` double DEFAULT NULL,
  `ae` double DEFAULT NULL,
  `date` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_res`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `results` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `pass` text,
  `foto` text,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

/*Table structure for table `v_results` */

DROP TABLE IF EXISTS `v_results`;

/*!50001 DROP VIEW IF EXISTS `v_results` */;
/*!50001 DROP TABLE IF EXISTS `v_results` */;

/*!50001 CREATE TABLE  `v_results`(
 `nama` varchar(30) ,
 `email` varchar(40) ,
 `ce` double ,
 `ro` double ,
 `ac` double ,
 `ae` double ,
 `date` varchar(30) 
)*/;

/*View structure for view v_results */

/*!50001 DROP TABLE IF EXISTS `v_results` */;
/*!50001 DROP VIEW IF EXISTS `v_results` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_results` AS select `a`.`nama` AS `nama`,`a`.`email` AS `email`,`b`.`ce` AS `ce`,`b`.`ro` AS `ro`,`b`.`ac` AS `ac`,`b`.`ae` AS `ae`,`b`.`date` AS `date` from (`register` `a` join `results` `b` on((`a`.`id_reg` = `b`.`id_reg`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
